﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UcPengembalian
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCari = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.txtCari = New System.Windows.Forms.TextBox()
        Me.dataGrid = New System.Windows.Forms.DataGridView()
        Me.id_pengembalian = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.judul_buku = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pengarang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.penerbit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nim = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tanggal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tanggal_pengembalian = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.denda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCari
        '
        Me.btnCari.Location = New System.Drawing.Point(278, 26)
        Me.btnCari.Name = "btnCari"
        Me.btnCari.Size = New System.Drawing.Size(96, 37)
        Me.btnCari.TabIndex = 15
        Me.btnCari.Text = "Cari"
        Me.btnCari.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Location = New System.Drawing.Point(1142, 26)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(96, 37)
        Me.btnAdd.TabIndex = 13
        Me.btnAdd.Text = "Tambah"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'txtCari
        '
        Me.txtCari.Location = New System.Drawing.Point(20, 31)
        Me.txtCari.Name = "txtCari"
        Me.txtCari.Size = New System.Drawing.Size(234, 26)
        Me.txtCari.TabIndex = 14
        '
        'dataGrid
        '
        Me.dataGrid.AllowUserToAddRows = False
        Me.dataGrid.AllowUserToDeleteRows = False
        Me.dataGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id_pengembalian, Me.code, Me.judul_buku, Me.pengarang, Me.penerbit, Me.nama, Me.nim, Me.tanggal, Me.tanggal_pengembalian, Me.denda})
        Me.dataGrid.Location = New System.Drawing.Point(20, 72)
        Me.dataGrid.Name = "dataGrid"
        Me.dataGrid.ReadOnly = True
        Me.dataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader
        Me.dataGrid.RowTemplate.Height = 28
        Me.dataGrid.Size = New System.Drawing.Size(1218, 552)
        Me.dataGrid.TabIndex = 12
        '
        'id_pengembalian
        '
        Me.id_pengembalian.DataPropertyName = "id_pengembalian"
        Me.id_pengembalian.HeaderText = "id_pengembalian"
        Me.id_pengembalian.Name = "id_pengembalian"
        Me.id_pengembalian.ReadOnly = True
        Me.id_pengembalian.Visible = False
        '
        'code
        '
        Me.code.DataPropertyName = "code"
        Me.code.FillWeight = 121.8274!
        Me.code.HeaderText = "Code Peminjaman"
        Me.code.Name = "code"
        Me.code.ReadOnly = True
        '
        'judul_buku
        '
        Me.judul_buku.DataPropertyName = "judul_buku"
        Me.judul_buku.FillWeight = 121.8274!
        Me.judul_buku.HeaderText = "Judul Buku"
        Me.judul_buku.Name = "judul_buku"
        Me.judul_buku.ReadOnly = True
        '
        'pengarang
        '
        Me.pengarang.DataPropertyName = "pengarang"
        Me.pengarang.FillWeight = 121.8274!
        Me.pengarang.HeaderText = "Pengarang"
        Me.pengarang.Name = "pengarang"
        Me.pengarang.ReadOnly = True
        '
        'penerbit
        '
        Me.penerbit.DataPropertyName = "penerbit"
        Me.penerbit.FillWeight = 121.8274!
        Me.penerbit.HeaderText = "Penerbit"
        Me.penerbit.Name = "penerbit"
        Me.penerbit.ReadOnly = True
        '
        'nama
        '
        Me.nama.DataPropertyName = "nama"
        Me.nama.HeaderText = "Nama"
        Me.nama.Name = "nama"
        Me.nama.ReadOnly = True
        '
        'nim
        '
        Me.nim.DataPropertyName = "nim"
        Me.nim.HeaderText = "NIM"
        Me.nim.Name = "nim"
        Me.nim.ReadOnly = True
        '
        'tanggal
        '
        Me.tanggal.DataPropertyName = "tanggal_peminjaman"
        Me.tanggal.FillWeight = 121.8274!
        Me.tanggal.HeaderText = "Tanggal Peminjaman"
        Me.tanggal.Name = "tanggal"
        Me.tanggal.ReadOnly = True
        '
        'tanggal_pengembalian
        '
        Me.tanggal_pengembalian.DataPropertyName = "tanggal_pengembalian"
        Me.tanggal_pengembalian.HeaderText = "Tanggal Pengembalian"
        Me.tanggal_pengembalian.Name = "tanggal_pengembalian"
        Me.tanggal_pengembalian.ReadOnly = True
        '
        'denda
        '
        Me.denda.DataPropertyName = "denda"
        Me.denda.HeaderText = "Denda"
        Me.denda.Name = "denda"
        Me.denda.ReadOnly = True
        '
        'UcPengembalian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnCari)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.txtCari)
        Me.Controls.Add(Me.dataGrid)
        Me.Name = "UcPengembalian"
        Me.Size = New System.Drawing.Size(1258, 650)
        CType(Me.dataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCari As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents txtCari As System.Windows.Forms.TextBox
    Friend WithEvents dataGrid As System.Windows.Forms.DataGridView
    Friend WithEvents id_pengembalian As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents judul_buku As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pengarang As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents penerbit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nim As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tanggal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tanggal_pengembalian As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents denda As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
