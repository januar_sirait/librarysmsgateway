﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormUtama
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormUtama))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.tsbHome = New System.Windows.Forms.ToolStripButton()
        Me.tsbConnection = New System.Windows.Forms.ToolStripButton()
        Me.tsbBuku = New System.Windows.Forms.ToolStripButton()
        Me.tsbAnggota = New System.Windows.Forms.ToolStripButton()
        Me.tsbPeminjaman = New System.Windows.Forms.ToolStripButton()
        Me.tsbPengembalian = New System.Windows.Forms.ToolStripButton()
        Me.tsbLaporan = New System.Windows.Forms.ToolStripSplitButton()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.statusBar = New System.Windows.Forms.ToolStripStatusLabel()
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel()
        Me.panelContent = New System.Windows.Forms.Panel()
        Me.ToolStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbHome, Me.tsbConnection, Me.tsbBuku, Me.tsbAnggota, Me.tsbPeminjaman, Me.tsbPengembalian, Me.tsbLaporan})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Padding = New System.Windows.Forms.Padding(0, 0, 2, 0)
        Me.ToolStrip1.Size = New System.Drawing.Size(1258, 32)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'tsbHome
        '
        Me.tsbHome.Image = Global.LibrarySmsGateway.My.Resources.Resources.Home
        Me.tsbHome.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbHome.Name = "tsbHome"
        Me.tsbHome.Size = New System.Drawing.Size(96, 29)
        Me.tsbHome.Text = "Beranda"
        Me.tsbHome.Visible = False
        '
        'tsbConnection
        '
        Me.tsbConnection.Image = Global.LibrarySmsGateway.My.Resources.Resources.High_Connection
        Me.tsbConnection.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbConnection.Name = "tsbConnection"
        Me.tsbConnection.Size = New System.Drawing.Size(93, 29)
        Me.tsbConnection.Text = "Koneksi"
        '
        'tsbBuku
        '
        Me.tsbBuku.Image = Global.LibrarySmsGateway.My.Resources.Resources.Books
        Me.tsbBuku.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbBuku.Name = "tsbBuku"
        Me.tsbBuku.Size = New System.Drawing.Size(71, 29)
        Me.tsbBuku.Text = "Buku"
        '
        'tsbAnggota
        '
        Me.tsbAnggota.Image = Global.LibrarySmsGateway.My.Resources.Resources.Conference
        Me.tsbAnggota.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbAnggota.Name = "tsbAnggota"
        Me.tsbAnggota.Size = New System.Drawing.Size(102, 29)
        Me.tsbAnggota.Text = "Anggota"
        '
        'tsbPeminjaman
        '
        Me.tsbPeminjaman.Image = Global.LibrarySmsGateway.My.Resources.Resources.Borrow_Book
        Me.tsbPeminjaman.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPeminjaman.Name = "tsbPeminjaman"
        Me.tsbPeminjaman.Size = New System.Drawing.Size(128, 29)
        Me.tsbPeminjaman.Text = "Peminjaman"
        '
        'tsbPengembalian
        '
        Me.tsbPengembalian.Image = CType(resources.GetObject("tsbPengembalian.Image"), System.Drawing.Image)
        Me.tsbPengembalian.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPengembalian.Name = "tsbPengembalian"
        Me.tsbPengembalian.Size = New System.Drawing.Size(143, 29)
        Me.tsbPengembalian.Text = "Pengembalian"
        '
        'tsbLaporan
        '
        Me.tsbLaporan.Image = Global.LibrarySmsGateway.My.Resources.Resources.Combo_Chart
        Me.tsbLaporan.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbLaporan.Name = "tsbLaporan"
        Me.tsbLaporan.Size = New System.Drawing.Size(108, 29)
        Me.tsbLaporan.Text = "Laporan"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.statusBar})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 682)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(2, 0, 21, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(1258, 30)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'statusBar
        '
        Me.statusBar.Name = "statusBar"
        Me.statusBar.Size = New System.Drawing.Size(181, 25)
        Me.statusBar.Text = "Status : Disconnected"
        '
        'BottomToolStripPanel
        '
        Me.BottomToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.BottomToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'TopToolStripPanel
        '
        Me.TopToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.TopToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'RightToolStripPanel
        '
        Me.RightToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.RightToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'LeftToolStripPanel
        '
        Me.LeftToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.LeftToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'ContentPanel
        '
        Me.ContentPanel.Size = New System.Drawing.Size(150, 150)
        '
        'panelContent
        '
        Me.panelContent.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelContent.Location = New System.Drawing.Point(0, 32)
        Me.panelContent.Name = "panelContent"
        Me.panelContent.Size = New System.Drawing.Size(1258, 650)
        Me.panelContent.TabIndex = 2
        '
        'FormUtama
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1258, 712)
        Me.Controls.Add(Me.panelContent)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "FormUtama"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aplikasi SMS Gateway Perpustakaan"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents tsbConnection As System.Windows.Forms.ToolStripButton
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents BottomToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents TopToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents RightToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents LeftToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents ContentPanel As System.Windows.Forms.ToolStripContentPanel
    Friend WithEvents tsbBuku As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAnggota As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbPeminjaman As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbPengembalian As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbLaporan As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents statusBar As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsbHome As System.Windows.Forms.ToolStripButton
    Friend WithEvents panelContent As System.Windows.Forms.Panel

End Class
