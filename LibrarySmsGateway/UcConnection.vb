﻿Imports System.IO.Ports
Imports System.Management

Public Class UcConnection
    Dim port As SerialPort
    Dim SMSGateway As SMSLib
    Dim formUtama As FormUtama

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        SMSGateway = New SMSLib()
    End Sub

    Private Sub UcConnection_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            Dim portsNames As String() = SerialPort.GetPortNames()
            For Each item As String In portsNames
                cmbPort.Items.Add(item)
            Next

            Try
                Dim searcher As New ManagementObjectSearcher( _
               "root\cimv2", _
               "SELECT * FROM Win32_SerialPort")

                For Each queryObj As ManagementObject In searcher.Get()
                    MsgBox(queryObj("Description"))
                Next

            Catch err As ManagementException
                MessageBox.Show("An error occurred while querying for WMI data: " & err.Message)
            End Try

            formUtama = Me.Parent.Parent
            port = formUtama.smsBackgroundProcess.port

            rdbSIM.Checked = True
            formUtama.smsBackgroundProcess.SIM_STORAGE = True
        Catch ex As Exception
            General.ErrorLog(ex.Message)
        End Try
    End Sub

    Private Sub rdbSIM_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbSIM.CheckedChanged
        formUtama.smsBackgroundProcess.SIM_STORAGE = True
    End Sub

    Private Sub rdbDevice_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbDevice.CheckedChanged
        formUtama.smsBackgroundProcess.SIM_STORAGE = False
    End Sub

    Private Sub btnConnect_Click(sender As System.Object, e As System.EventArgs) Handles btnConnect.Click
        Try
            If btnConnect.Text.Equals("Connect") Then
                Me.port = formUtama.smsBackgroundProcess.smsLib.OpenPort(cmbPort.Text,
                                                                         Integer.Parse(cmbBaudRate.Text),
                                                                         Integer.Parse(cmbDataBits.Text),
                                                                         Integer.Parse(cmbReadTimeout.Text),
                                                                         Integer.Parse(cmbWriteTimeout.Text))

                If Not IsNothing(Me.port) Then
                    formUtama.statusBar.Text = "Status : Connected to " + cmbPort.Text
                Else
                    formUtama.statusBar.Text = "Status : Invalid port settings"
                End If
                formUtama.smsBackgroundProcess.port = Me.port
                btnConnect.Text = "Disconnect"
                gboPortSetting.Enabled = False
            Else
                formUtama.smsBackgroundProcess.ClosePort()
                btnConnect.Text = "Connect"
                gboPortSetting.Enabled = True
            End If
        Catch ex As Exception
            formUtama.statusBar.Text = "Status : " & ex.Message
        End Try
    End Sub
End Class
