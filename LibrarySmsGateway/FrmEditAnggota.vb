﻿Public Class FrmEditAnggota
    Dim database As DatabaseEntities
    Dim IsEdit As Boolean

    Public anggota As anggota

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        database = New DatabaseEntities()
        IsEdit = False
        anggota = New anggota()
    End Sub

    Private Sub FrmEditAnggota_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Dim fakultas = (From f In database.fakultas
                       Select f).ToList()

        cmbFakultas.DataSource = fakultas
        cmbFakultas.ValueMember = "nama"

        Dim prodi = (From p In database.prodi
                         Where p.id_fakultas = anggota.prodi.id_fakultas
                         Select p).ToList()

        cmbProdi.DataSource = prodi
        cmbProdi.ValueMember = "nama"

        txtNama.Text = anggota.nama
        txtNIM.Text = anggota.nim
        cmbFakultas.SelectedValue = anggota.prodi.fakultas.nama
        cmbProdi.SelectedValue = anggota.prodi.nama
        txtTmpLahir.Text = anggota.tempat_lahir
        dtpTanggalLahir.Value = anggota.tanggal_lahir
        txtNoHp.Text = anggota.no_hp

        txtNama.Enabled = IsEdit
        txtNIM.Enabled = IsEdit
        cmbFakultas.Enabled = IsEdit
        cmbProdi.Enabled = IsEdit
        txtTmpLahir.Enabled = IsEdit
        dtpTanggalLahir.Enabled = IsEdit
        txtNoHp.Enabled = IsEdit

        btnSave.Text = "&Edit"
    End Sub

    Private Sub btnBatal_Click(sender As System.Object, e As System.EventArgs) Handles btnBatal.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If IsEdit Then
            If txtNama.Text = "" Then
                General.ErrorMessage("Nama tidak boleh kosong!")
                Return
            End If

            If txtNIM.Text = "" Then
                General.ErrorMessage("Nim tidak boleh kosong!")
                Return
            End If

            If cmbFakultas.SelectedIndex < 0 Then
                General.ErrorMessage("Silahkan pilih Fakultas Anda!")
                Return
            End If

            If cmbProdi.SelectedIndex < 0 Then
                General.ErrorMessage("Silahkan pilih Program Studi Anda!")
                Return
            End If

            If txtTmpLahir.Text = "" Then
                General.ErrorMessage("Tempat lahir tidak boleh kosong!")
                Return
            End If

            If txtNoHp.Text = "" Then
                General.ErrorMessage("No hp tidak boleh kosong!")
                Return
            End If
            Dim prodi As prodi = cmbProdi.SelectedItem

            Dim temp As anggota = (From a In database.anggota
                                   Where a.id_anggota = anggota.id_anggota
                                   Select a).First()

            temp.nama = txtNama.Text
            temp.nim = txtNIM.Text
            temp.id_prodi = prodi.id_prodi
            temp.tempat_lahir = txtTmpLahir.Text
            temp.tanggal_lahir = dtpTanggalLahir.Value
            temp.no_hp = txtNoHp.Text

            If IsNothing(temp.EntityKey) Or temp.EntityKey.IsTemporary Then
                database.anggota.AddObject(temp)
            End If
            database.ObjectStateManager.ChangeObjectState(temp, EntityState.Modified)
            database.SaveChanges()

            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            IsEdit = True
            txtNama.Enabled = IsEdit
            txtNIM.Enabled = IsEdit
            cmbFakultas.Enabled = IsEdit
            cmbProdi.Enabled = IsEdit
            txtTmpLahir.Enabled = IsEdit
            dtpTanggalLahir.Enabled = IsEdit
            txtNoHp.Enabled = IsEdit

            btnSave.Text = "&Simpan"
        End If

    End Sub

    Private Sub cmbFakultas_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbFakultas.SelectedIndexChanged
        Dim selectedFakultas As fakultas = cmbFakultas.SelectedItem

        Dim prodi = (From p In database.prodi
                     Where p.id_fakultas = selectedFakultas.id_fakultas
                     Select p).ToList()

        cmbProdi.DataSource = prodi
    End Sub
End Class