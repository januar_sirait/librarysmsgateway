﻿Public Class FrmSearchAnggota
    Dim database As DatabaseEntities
    Public anggota As anggota

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
        anggota = New anggota()
    End Sub

    Private Sub LoadData()
        Dim search As String = txtCari.Text
        Dim data
        If search = "" Then
            data = (From a In database.anggota
                   Select id_anggota = a.id_anggota,
                   nim = a.nim,
                   nama = a.nama,
                   fakultas = a.prodi.fakultas.nama,
                   prodi = a.prodi.nama,
                   tempat_lahir = a.tempat_lahir,
                   tanggal_lahir = a.tanggal_lahir,
                   no_hp = a.no_hp).ToList()
        Else
            data = (From a In database.anggota
                    Where a.nama.ToLower().Contains(search.ToLower())
                   Select id_anggota = a.id_anggota,
                   nim = a.nim,
                   nama = a.nama,
                   fakultas = a.prodi.fakultas.nama,
                   prodi = a.prodi.nama,
                   tempat_lahir = a.tempat_lahir,
                   tanggal_lahir = a.tanggal_lahir,
                   no_hp = a.no_hp).ToList()
        End If
        dataGrid.DataSource = data
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub BtnCari_Click(sender As System.Object, e As System.EventArgs) Handles BtnCari.Click
        LoadData()
    End Sub

    Private Sub dataGrid_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dataGrid.CellDoubleClick
        Dim id_anggota As Integer = dataGrid.Rows(e.RowIndex).Cells(0).Value
        anggota = (From a In database.anggota
                                 Where a.id_anggota = id_anggota
                                 Select a).FirstOrDefault()

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub FrmSearchAnggota_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadData()
    End Sub
End Class