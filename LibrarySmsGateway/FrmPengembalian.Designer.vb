﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPengembalian
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.panelBody = New System.Windows.Forms.Panel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtAnggotaNim = New System.Windows.Forms.TextBox()
        Me.txtAnggotaHp = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtAnggotaProdi = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtAnggotaFakultas = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtAnggotaNama = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtBukuJudul = New System.Windows.Forms.TextBox()
        Me.txtBukuUnit = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtBukuPenerbit = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtBukuPengarang = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtDenda = New System.Windows.Forms.TextBox()
        Me.txtTanggalPeminjaman = New System.Windows.Forms.TextBox()
        Me.cmbCode = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.panelFooter = New System.Windows.Forms.Panel()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnBatal = New System.Windows.Forms.Button()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.panelHeader = New System.Windows.Forms.Panel()
        Me.panelBody.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.panelFooter.SuspendLayout()
        Me.panelHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelBody
        '
        Me.panelBody.Controls.Add(Me.GroupBox3)
        Me.panelBody.Controls.Add(Me.GroupBox2)
        Me.panelBody.Controls.Add(Me.GroupBox1)
        Me.panelBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelBody.Location = New System.Drawing.Point(0, 80)
        Me.panelBody.Name = "panelBody"
        Me.panelBody.Size = New System.Drawing.Size(1057, 421)
        Me.panelBody.TabIndex = 15
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtAnggotaNim)
        Me.GroupBox3.Controls.Add(Me.txtAnggotaHp)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.txtAnggotaProdi)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.txtAnggotaFakultas)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.txtAnggotaNama)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Location = New System.Drawing.Point(591, 175)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(440, 234)
        Me.GroupBox3.TabIndex = 8
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Data Anggota"
        '
        'txtAnggotaNim
        '
        Me.txtAnggotaNim.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnggotaNim.Location = New System.Drawing.Point(140, 36)
        Me.txtAnggotaNim.Name = "txtAnggotaNim"
        Me.txtAnggotaNim.ReadOnly = True
        Me.txtAnggotaNim.Size = New System.Drawing.Size(283, 26)
        Me.txtAnggotaNim.TabIndex = 12
        '
        'txtAnggotaHp
        '
        Me.txtAnggotaHp.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnggotaHp.Location = New System.Drawing.Point(140, 176)
        Me.txtAnggotaHp.Name = "txtAnggotaHp"
        Me.txtAnggotaHp.ReadOnly = True
        Me.txtAnggotaHp.Size = New System.Drawing.Size(283, 26)
        Me.txtAnggotaHp.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 179)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(117, 20)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "No Handphone"
        '
        'txtAnggotaProdi
        '
        Me.txtAnggotaProdi.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnggotaProdi.Location = New System.Drawing.Point(140, 140)
        Me.txtAnggotaProdi.Name = "txtAnggotaProdi"
        Me.txtAnggotaProdi.ReadOnly = True
        Me.txtAnggotaProdi.Size = New System.Drawing.Size(283, 26)
        Me.txtAnggotaProdi.TabIndex = 7
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(19, 143)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(110, 20)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Program Studi"
        '
        'txtAnggotaFakultas
        '
        Me.txtAnggotaFakultas.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnggotaFakultas.Location = New System.Drawing.Point(140, 106)
        Me.txtAnggotaFakultas.Name = "txtAnggotaFakultas"
        Me.txtAnggotaFakultas.ReadOnly = True
        Me.txtAnggotaFakultas.Size = New System.Drawing.Size(283, 26)
        Me.txtAnggotaFakultas.TabIndex = 5
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(19, 109)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 20)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Fakultas"
        '
        'txtAnggotaNama
        '
        Me.txtAnggotaNama.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnggotaNama.Location = New System.Drawing.Point(140, 71)
        Me.txtAnggotaNama.Name = "txtAnggotaNama"
        Me.txtAnggotaNama.ReadOnly = True
        Me.txtAnggotaNama.Size = New System.Drawing.Size(283, 26)
        Me.txtAnggotaNama.TabIndex = 3
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(19, 74)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(51, 20)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Nama"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(19, 39)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(38, 20)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "NIM"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtBukuJudul)
        Me.GroupBox2.Controls.Add(Me.txtBukuUnit)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txtBukuPenerbit)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtBukuPengarang)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Location = New System.Drawing.Point(24, 175)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(548, 234)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Data Buku"
        '
        'txtBukuJudul
        '
        Me.txtBukuJudul.BackColor = System.Drawing.SystemColors.Window
        Me.txtBukuJudul.Location = New System.Drawing.Point(140, 36)
        Me.txtBukuJudul.Name = "txtBukuJudul"
        Me.txtBukuJudul.ReadOnly = True
        Me.txtBukuJudul.Size = New System.Drawing.Size(393, 26)
        Me.txtBukuJudul.TabIndex = 10
        '
        'txtBukuUnit
        '
        Me.txtBukuUnit.BackColor = System.Drawing.SystemColors.Window
        Me.txtBukuUnit.Location = New System.Drawing.Point(140, 140)
        Me.txtBukuUnit.Name = "txtBukuUnit"
        Me.txtBukuUnit.ReadOnly = True
        Me.txtBukuUnit.Size = New System.Drawing.Size(124, 26)
        Me.txtBukuUnit.TabIndex = 7
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(19, 143)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(38, 20)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Unit"
        '
        'txtBukuPenerbit
        '
        Me.txtBukuPenerbit.BackColor = System.Drawing.SystemColors.Window
        Me.txtBukuPenerbit.Location = New System.Drawing.Point(140, 105)
        Me.txtBukuPenerbit.Name = "txtBukuPenerbit"
        Me.txtBukuPenerbit.ReadOnly = True
        Me.txtBukuPenerbit.Size = New System.Drawing.Size(393, 26)
        Me.txtBukuPenerbit.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(19, 108)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 20)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Penerbit"
        '
        'txtBukuPengarang
        '
        Me.txtBukuPengarang.BackColor = System.Drawing.SystemColors.Window
        Me.txtBukuPengarang.Location = New System.Drawing.Point(140, 71)
        Me.txtBukuPengarang.Name = "txtBukuPengarang"
        Me.txtBukuPengarang.ReadOnly = True
        Me.txtBukuPengarang.Size = New System.Drawing.Size(393, 26)
        Me.txtBukuPengarang.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(19, 74)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 20)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Pengarang"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(19, 39)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 20)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Judul Buku"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtDenda)
        Me.GroupBox1.Controls.Add(Me.txtTanggalPeminjaman)
        Me.GroupBox1.Controls.Add(Me.cmbCode)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(24, 19)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1007, 138)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Peminjaman"
        '
        'txtDenda
        '
        Me.txtDenda.BackColor = System.Drawing.SystemColors.Window
        Me.txtDenda.Location = New System.Drawing.Point(234, 103)
        Me.txtDenda.Name = "txtDenda"
        Me.txtDenda.ReadOnly = True
        Me.txtDenda.Size = New System.Drawing.Size(178, 26)
        Me.txtDenda.TabIndex = 9
        '
        'txtTanggalPeminjaman
        '
        Me.txtTanggalPeminjaman.BackColor = System.Drawing.SystemColors.Window
        Me.txtTanggalPeminjaman.Location = New System.Drawing.Point(234, 67)
        Me.txtTanggalPeminjaman.Name = "txtTanggalPeminjaman"
        Me.txtTanggalPeminjaman.ReadOnly = True
        Me.txtTanggalPeminjaman.Size = New System.Drawing.Size(178, 26)
        Me.txtTanggalPeminjaman.TabIndex = 8
        '
        'cmbCode
        '
        Me.cmbCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCode.FormattingEnabled = True
        Me.cmbCode.Location = New System.Drawing.Point(234, 29)
        Me.cmbCode.Name = "cmbCode"
        Me.cmbCode.Size = New System.Drawing.Size(206, 28)
        Me.cmbCode.TabIndex = 4
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(19, 106)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(96, 20)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Total Denda"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(157, 20)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Tanggal Peminjaman"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Kode Peminjaman"
        '
        'panelFooter
        '
        Me.panelFooter.Controls.Add(Me.btnSave)
        Me.panelFooter.Controls.Add(Me.btnBatal)
        Me.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelFooter.Location = New System.Drawing.Point(0, 501)
        Me.panelFooter.Name = "panelFooter"
        Me.panelFooter.Size = New System.Drawing.Size(1057, 87)
        Me.panelFooter.TabIndex = 14
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(922, 24)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(109, 41)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Simpan"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnBatal
        '
        Me.btnBatal.Location = New System.Drawing.Point(790, 24)
        Me.btnBatal.Name = "btnBatal"
        Me.btnBatal.Size = New System.Drawing.Size(109, 41)
        Me.btnBatal.TabIndex = 1
        Me.btnBatal.Text = "Batal"
        Me.btnBatal.UseVisualStyleBackColor = True
        '
        'lblHeader
        '
        Me.lblHeader.AutoSize = True
        Me.lblHeader.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblHeader.Location = New System.Drawing.Point(18, 32)
        Me.lblHeader.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(188, 36)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Pengembalian"
        '
        'panelHeader
        '
        Me.panelHeader.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.panelHeader.Controls.Add(Me.lblHeader)
        Me.panelHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelHeader.Location = New System.Drawing.Point(0, 0)
        Me.panelHeader.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.panelHeader.Name = "panelHeader"
        Me.panelHeader.Size = New System.Drawing.Size(1057, 80)
        Me.panelHeader.TabIndex = 13
        '
        'FrmPengembalian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1057, 588)
        Me.Controls.Add(Me.panelBody)
        Me.Controls.Add(Me.panelFooter)
        Me.Controls.Add(Me.panelHeader)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmPengembalian"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "FrmPengembalian"
        Me.panelBody.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.panelFooter.ResumeLayout(False)
        Me.panelHeader.ResumeLayout(False)
        Me.panelHeader.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panelBody As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtAnggotaNim As System.Windows.Forms.TextBox
    Friend WithEvents txtAnggotaHp As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtAnggotaProdi As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtAnggotaFakultas As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtAnggotaNama As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtBukuJudul As System.Windows.Forms.TextBox
    Friend WithEvents txtBukuUnit As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtBukuPenerbit As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtBukuPengarang As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents panelFooter As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnBatal As System.Windows.Forms.Button
    Private WithEvents lblHeader As System.Windows.Forms.Label
    Private WithEvents panelHeader As System.Windows.Forms.Panel
    Friend WithEvents txtDenda As System.Windows.Forms.TextBox
    Friend WithEvents txtTanggalPeminjaman As System.Windows.Forms.TextBox
    Friend WithEvents cmbCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
End Class
