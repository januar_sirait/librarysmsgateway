﻿Public Class FrmAddAnggota
    Dim database As DatabaseEntities

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If txtNama.Text = "" Then
            General.ErrorMessage("Nama tidak boleh kosong!")
            Return
        End If

        If txtNIM.Text = "" Then
            General.ErrorMessage("Nim tidak boleh kosong!")
            Return
        End If

        If cmbFakultas.SelectedIndex < 0 Then
            General.ErrorMessage("Silahkan pilih Fakultas Anda!")
            Return
        End If

        If cmbProdi.SelectedIndex < 0 Then
            General.ErrorMessage("Silahkan pilih Program Studi Anda!")
            Return
        End If

        If txtTmpLahir.Text = "" Then
            General.ErrorMessage("Tempat lahir tidak boleh kosong!")
            Return
        End If

        If txtNoHp.Text = "" Then
            General.ErrorMessage("No hp tidak boleh kosong!")
            Return
        End If

        Try
            Dim prodi As prodi = cmbProdi.SelectedItem

            Dim anggota As anggota = New anggota()
            anggota.nama = txtNama.Text
            anggota.nim = txtNIM.Text
            anggota.id_prodi = prodi.id_prodi
            anggota.tempat_lahir = txtTmpLahir.Text
            anggota.tanggal_lahir = dtpTanggalLahir.Value
            anggota.no_hp = txtNoHp.Text

            database.anggota.AddObject(anggota)
            database.SaveChanges()

            Me.DialogResult = Windows.Forms.DialogResult.OK
        Catch ex As Exception
            General.ErrorMessage(ex.Message)
        End Try
    End Sub

    Private Sub btnBatal_Click(sender As System.Object, e As System.EventArgs) Handles btnBatal.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub FrmAddAnggota_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim fakultas = (From f In database.fakultas
                       Select f).ToList()

        cmbFakultas.DataSource = fakultas
        cmbFakultas.ValueMember = "nama"
        cmbFakultas.SelectedIndex = -1

        cmbProdi.SelectedIndex = -1
        cmbProdi.Enabled = False
    End Sub

    Private Sub cmbFakultas_SelectedValueChanged(sender As System.Object, e As System.EventArgs) Handles cmbFakultas.SelectedValueChanged
        If cmbFakultas.SelectedIndex > -1 Then
            Dim selectedFakultas As fakultas = cmbFakultas.SelectedItem

            Dim prodi = (From p In database.prodi
                         Where p.id_fakultas = selectedFakultas.id_fakultas
                         Select p).ToList()

            cmbProdi.DataSource = prodi
            cmbProdi.ValueMember = "nama"
            cmbProdi.SelectedIndex = -1
            cmbProdi.Enabled = True
        End If
    End Sub
End Class