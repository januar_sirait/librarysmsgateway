﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UcAnggota
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCari = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.txtCari = New System.Windows.Forms.TextBox()
        Me.dataGrid = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.judul = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pengarang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.penerbit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.unit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tanggal_lahir = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.no_hp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCari
        '
        Me.btnCari.Location = New System.Drawing.Point(278, 26)
        Me.btnCari.Name = "btnCari"
        Me.btnCari.Size = New System.Drawing.Size(96, 37)
        Me.btnCari.TabIndex = 7
        Me.btnCari.Text = "Cari"
        Me.btnCari.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Location = New System.Drawing.Point(1142, 26)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(96, 37)
        Me.btnAdd.TabIndex = 5
        Me.btnAdd.Text = "Tambah"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'txtCari
        '
        Me.txtCari.Location = New System.Drawing.Point(20, 31)
        Me.txtCari.Name = "txtCari"
        Me.txtCari.Size = New System.Drawing.Size(234, 26)
        Me.txtCari.TabIndex = 6
        '
        'dataGrid
        '
        Me.dataGrid.AllowUserToAddRows = False
        Me.dataGrid.AllowUserToDeleteRows = False
        Me.dataGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.judul, Me.pengarang, Me.penerbit, Me.unit, Me.status, Me.tanggal_lahir, Me.no_hp})
        Me.dataGrid.Location = New System.Drawing.Point(20, 72)
        Me.dataGrid.Name = "dataGrid"
        Me.dataGrid.ReadOnly = True
        Me.dataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader
        Me.dataGrid.RowTemplate.Height = 28
        Me.dataGrid.Size = New System.Drawing.Size(1218, 552)
        Me.dataGrid.TabIndex = 4
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "id_anggota"
        Me.Column1.HeaderText = "id_anggota"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Visible = False
        '
        'judul
        '
        Me.judul.DataPropertyName = "nim"
        Me.judul.FillWeight = 50.76142!
        Me.judul.HeaderText = "Nim"
        Me.judul.Name = "judul"
        Me.judul.ReadOnly = True
        '
        'pengarang
        '
        Me.pengarang.DataPropertyName = "nama"
        Me.pengarang.FillWeight = 112.3096!
        Me.pengarang.HeaderText = "Nama"
        Me.pengarang.Name = "pengarang"
        Me.pengarang.ReadOnly = True
        '
        'penerbit
        '
        Me.penerbit.DataPropertyName = "fakultas"
        Me.penerbit.FillWeight = 112.3096!
        Me.penerbit.HeaderText = "Fakultas"
        Me.penerbit.Name = "penerbit"
        Me.penerbit.ReadOnly = True
        '
        'unit
        '
        Me.unit.DataPropertyName = "prodi"
        Me.unit.FillWeight = 112.3096!
        Me.unit.HeaderText = "Program Studi"
        Me.unit.Name = "unit"
        Me.unit.ReadOnly = True
        '
        'status
        '
        Me.status.DataPropertyName = "tempat_lahir"
        Me.status.HeaderText = "Tempat Lahir"
        Me.status.Name = "status"
        Me.status.ReadOnly = True
        Me.status.Visible = False
        '
        'tanggal_lahir
        '
        Me.tanggal_lahir.DataPropertyName = "tanggal_lahir"
        Me.tanggal_lahir.HeaderText = "Tanggal Lahir"
        Me.tanggal_lahir.Name = "tanggal_lahir"
        Me.tanggal_lahir.ReadOnly = True
        Me.tanggal_lahir.Visible = False
        '
        'no_hp
        '
        Me.no_hp.DataPropertyName = "no_hp"
        Me.no_hp.FillWeight = 112.3096!
        Me.no_hp.HeaderText = "No Handphone"
        Me.no_hp.Name = "no_hp"
        Me.no_hp.ReadOnly = True
        '
        'UcAnggota
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnCari)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.txtCari)
        Me.Controls.Add(Me.dataGrid)
        Me.Name = "UcAnggota"
        Me.Size = New System.Drawing.Size(1258, 650)
        CType(Me.dataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCari As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents txtCari As System.Windows.Forms.TextBox
    Friend WithEvents dataGrid As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents judul As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pengarang As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents penerbit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents unit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tanggal_lahir As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents no_hp As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
