﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEditAnggota
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtNoHp = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtTmpLahir = New System.Windows.Forms.TextBox()
        Me.panelHeader = New System.Windows.Forms.Panel()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnBatal = New System.Windows.Forms.Button()
        Me.panelBody = New System.Windows.Forms.Panel()
        Me.dtpTanggalLahir = New System.Windows.Forms.DateTimePicker()
        Me.cmbProdi = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbFakultas = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNIM = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNama = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.panelFooter = New System.Windows.Forms.Panel()
        Me.panelHeader.SuspendLayout()
        Me.panelBody.SuspendLayout()
        Me.panelFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtNoHp
        '
        Me.txtNoHp.Location = New System.Drawing.Point(151, 369)
        Me.txtNoHp.Name = "txtNoHp"
        Me.txtNoHp.Size = New System.Drawing.Size(304, 26)
        Me.txtNoHp.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(31, 329)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(105, 20)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Tanggal Lahir"
        '
        'txtTmpLahir
        '
        Me.txtTmpLahir.Location = New System.Drawing.Point(151, 281)
        Me.txtTmpLahir.Name = "txtTmpLahir"
        Me.txtTmpLahir.Size = New System.Drawing.Size(304, 26)
        Me.txtTmpLahir.TabIndex = 11
        '
        'panelHeader
        '
        Me.panelHeader.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.panelHeader.Controls.Add(Me.lblHeader)
        Me.panelHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelHeader.Location = New System.Drawing.Point(0, 0)
        Me.panelHeader.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.panelHeader.Name = "panelHeader"
        Me.panelHeader.Size = New System.Drawing.Size(626, 80)
        Me.panelHeader.TabIndex = 10
        '
        'lblHeader
        '
        Me.lblHeader.AutoSize = True
        Me.lblHeader.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblHeader.Location = New System.Drawing.Point(18, 32)
        Me.lblHeader.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(171, 36)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Edit Anggota"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(31, 375)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(117, 20)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "No Handphone"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(31, 287)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(102, 20)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Tempat Lahir"
        '
        'btnBatal
        '
        Me.btnBatal.Location = New System.Drawing.Point(361, 23)
        Me.btnBatal.Name = "btnBatal"
        Me.btnBatal.Size = New System.Drawing.Size(109, 41)
        Me.btnBatal.TabIndex = 1
        Me.btnBatal.Text = "Batal"
        Me.btnBatal.UseVisualStyleBackColor = True
        '
        'panelBody
        '
        Me.panelBody.Controls.Add(Me.txtNoHp)
        Me.panelBody.Controls.Add(Me.Label8)
        Me.panelBody.Controls.Add(Me.dtpTanggalLahir)
        Me.panelBody.Controls.Add(Me.Label7)
        Me.panelBody.Controls.Add(Me.txtTmpLahir)
        Me.panelBody.Controls.Add(Me.Label6)
        Me.panelBody.Controls.Add(Me.cmbProdi)
        Me.panelBody.Controls.Add(Me.Label5)
        Me.panelBody.Controls.Add(Me.cmbFakultas)
        Me.panelBody.Controls.Add(Me.Label4)
        Me.panelBody.Controls.Add(Me.txtNIM)
        Me.panelBody.Controls.Add(Me.Label3)
        Me.panelBody.Controls.Add(Me.txtNama)
        Me.panelBody.Controls.Add(Me.Label2)
        Me.panelBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelBody.Location = New System.Drawing.Point(0, 0)
        Me.panelBody.Name = "panelBody"
        Me.panelBody.Size = New System.Drawing.Size(626, 424)
        Me.panelBody.TabIndex = 12
        '
        'dtpTanggalLahir
        '
        Me.dtpTanggalLahir.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTanggalLahir.Location = New System.Drawing.Point(151, 324)
        Me.dtpTanggalLahir.Name = "dtpTanggalLahir"
        Me.dtpTanggalLahir.Size = New System.Drawing.Size(214, 26)
        Me.dtpTanggalLahir.TabIndex = 13
        '
        'cmbProdi
        '
        Me.cmbProdi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProdi.FormattingEnabled = True
        Me.cmbProdi.Items.AddRange(New Object() {"Kedokteran", "Hukum"})
        Me.cmbProdi.Location = New System.Drawing.Point(151, 237)
        Me.cmbProdi.Name = "cmbProdi"
        Me.cmbProdi.Size = New System.Drawing.Size(304, 28)
        Me.cmbProdi.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(31, 245)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 20)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Program Studi"
        '
        'cmbFakultas
        '
        Me.cmbFakultas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFakultas.FormattingEnabled = True
        Me.cmbFakultas.Items.AddRange(New Object() {"Kedokteran", "Hukum"})
        Me.cmbFakultas.Location = New System.Drawing.Point(151, 190)
        Me.cmbFakultas.Name = "cmbFakultas"
        Me.cmbFakultas.Size = New System.Drawing.Size(304, 28)
        Me.cmbFakultas.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(31, 198)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 20)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Fakultas"
        '
        'txtNIM
        '
        Me.txtNIM.Location = New System.Drawing.Point(151, 148)
        Me.txtNIM.Name = "txtNIM"
        Me.txtNIM.Size = New System.Drawing.Size(425, 26)
        Me.txtNIM.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(31, 154)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 20)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "NIM"
        '
        'txtNama
        '
        Me.txtNama.Location = New System.Drawing.Point(151, 104)
        Me.txtNama.Name = "txtNama"
        Me.txtNama.Size = New System.Drawing.Size(425, 26)
        Me.txtNama.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 110)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 20)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Nama"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(493, 23)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(109, 41)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Simpan"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'panelFooter
        '
        Me.panelFooter.Controls.Add(Me.btnSave)
        Me.panelFooter.Controls.Add(Me.btnBatal)
        Me.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelFooter.Location = New System.Drawing.Point(0, 424)
        Me.panelFooter.Name = "panelFooter"
        Me.panelFooter.Size = New System.Drawing.Size(626, 87)
        Me.panelFooter.TabIndex = 11
        '
        'FrmEditAnggota
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(626, 511)
        Me.Controls.Add(Me.panelHeader)
        Me.Controls.Add(Me.panelBody)
        Me.Controls.Add(Me.panelFooter)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmEditAnggota"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "FrmEditAnggota"
        Me.panelHeader.ResumeLayout(False)
        Me.panelHeader.PerformLayout()
        Me.panelBody.ResumeLayout(False)
        Me.panelBody.PerformLayout()
        Me.panelFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtNoHp As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtTmpLahir As System.Windows.Forms.TextBox
    Private WithEvents panelHeader As System.Windows.Forms.Panel
    Private WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnBatal As System.Windows.Forms.Button
    Friend WithEvents panelBody As System.Windows.Forms.Panel
    Friend WithEvents dtpTanggalLahir As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbProdi As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbFakultas As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNIM As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNama As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents panelFooter As System.Windows.Forms.Panel
End Class
