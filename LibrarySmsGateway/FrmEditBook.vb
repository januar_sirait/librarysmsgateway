﻿Public Class FrmEditBook
    Public book As buku
    Dim database As DatabaseEntities

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        book = New buku
        database = New DatabaseEntities()
    End Sub

    Private Sub FrmEditBook_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        txtJudul.Text = book.judul_buku
        txtPengarang.Text = book.pengarang
        txtPenerbit.Text = book.penerbit
        cmbStatus.SelectedItem = IIf(book.status = True, "tersedia", "tidak tersedia")
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If txtJudul.Text = "" Then
            General.ErrorMessage("Judul buku tidak boleh kosong!")
            Return
        End If

        If txtPengarang.Text = "" Then
            General.ErrorMessage("Pengarang buku tidak boleh kosong!")
            Return
        End If

        If txtPenerbit.Text = "" Then
            General.ErrorMessage("Penerbit buku tidak boleh kosong!")
            Return
        End If

        If chkUbah.Checked Then
            Dim listBook = (From b In database.buku
                            Where (b.judul_buku = book.judul_buku) And (b.pengarang Is book.pengarang) And (b.penerbit Is book.penerbit)
                            Select b).ToList()
            For Each item As buku In listBook
                item.judul_buku = txtJudul.Text
                item.pengarang = txtPengarang.Text
                item.penerbit = txtPenerbit.Text
                item.status = If(cmbStatus.Text.Equals("tersedia"), True, False)
                database.ObjectStateManager.ChangeObjectState(item, EntityState.Modified)
            Next
        Else
            book.judul_buku = txtJudul.Text
            book.pengarang = txtPengarang.Text
            book.penerbit = txtPenerbit.Text
            book.status = If(cmbStatus.Text.Equals("tersedia"), True, False)
            database.AddTobuku(book)
            database.ObjectStateManager.ChangeObjectState(book, EntityState.Modified)
        End If
        database.SaveChanges()

        Me.DialogResult = Windows.Forms.DialogResult.OK

    End Sub

    Private Sub btnBatal_Click(sender As System.Object, e As System.EventArgs) Handles btnBatal.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
End Class