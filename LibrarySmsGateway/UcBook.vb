﻿Public Class UcBook
    Dim database As DatabaseEntities

    Private Sub UcBook_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        database = New DatabaseEntities()
    End Sub

    Public Sub LoadData()
        Dim search As String = txtCari.Text
        Dim data
        If search = "" Then
            data = (From b In database.buku
                   Select id_buku = b.id_buku,
                   judul_buku = b.judul_buku,
                   pengarang = b.pengarang,
                   penerbit = b.penerbit,
                   unit = b.unit,
                   status = If(b.status = True, "tersedia", "tidak tersedia")).ToList()
        Else
            data = (From b In database.buku
                    Where b.judul_buku.ToLower().Contains(search.ToLower()) Or b.pengarang.ToLower().Contains(search.ToLower())
                   Select id_buku = b.id_buku,
                   judul_buku = b.judul_buku,
                   pengarang = b.pengarang,
                   penerbit = b.penerbit,
                   unit = b.unit,
                   status = If(b.status = True, "tersedia", "tidak tersedia")).ToList()
        End If
        dataGrid.DataSource = data
    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click
        Dim frmAddBook = New FrmAddBook()
        Dim result As DialogResult
        result = frmAddBook.ShowDialog()

        If result = DialogResult.OK Then
            LoadData()
        End If
    End Sub

    Private Sub dataGrid_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dataGrid.CellDoubleClick
        
        Dim frmEdit As FrmEditBook = New FrmEditBook()
        frmEdit.book.id_buku = dataGrid.Rows(e.RowIndex).Cells(0).Value
        frmEdit.book.judul_buku = dataGrid.Rows(e.RowIndex).Cells(1).Value
        frmEdit.book.pengarang = dataGrid.Rows(e.RowIndex).Cells(2).Value
        frmEdit.book.penerbit = dataGrid.Rows(e.RowIndex).Cells(3).Value
        frmEdit.book.unit = dataGrid.Rows(e.RowIndex).Cells(4).Value
        frmEdit.book.status = If(dataGrid.Rows(e.RowIndex).Cells(5).Value.Equals("tersedia"), True, False)

        Dim result As DialogResult
        result = frmEdit.ShowDialog()
        If result = DialogResult.OK Then
            LoadData()
        End If
    End Sub

    Private Sub btnCari_Click(sender As System.Object, e As System.EventArgs) Handles btnCari.Click
        LoadData()
    End Sub
End Class
