﻿Imports System.IO.Ports
Imports System.Threading
Imports System.Text

Public Class SMSLib
    Public receiveNow As AutoResetEvent
    Shared readNow As AutoResetEvent = New AutoResetEvent(False)

    Public Function OpenPort(ByVal portName As String, ByVal baudRate As Integer, ByVal dataBits As Integer,
                             ByVal readTimeout As Integer, ByVal writeTimeout As Integer)
        receiveNow = New AutoResetEvent(True)
        Dim port As SerialPort = New SerialPort()
        Try
            port.PortName = portName
            port.BaudRate = baudRate
            port.DataBits = dataBits
            port.ReadTimeout = readTimeout
            port.WriteTimeout = writeTimeout

            port.StopBits = StopBits.One
            port.Parity = Parity.None
            port.Encoding = Encoding.GetEncoding("iso-8859-1")
            AddHandler port.DataReceived, AddressOf port_DataReceived
            port.Open()
            port.DtrEnable = True
            port.RtsEnable = True
        Catch ex As Exception
            General.ErrorMessage(ex.Message)
            Throw ex
        End Try

        Return port
    End Function

    Public Sub port_DataReceived(sender As Object, e As SerialDataReceivedEventArgs)
        Try
            If e.EventType = SerialData.Chars Then
                receiveNow.Set()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ReadResponse(port As SerialPort, timeout As Integer)
        Dim buffer As String = String.Empty
        Try
            Do
                If receiveNow.WaitOne(timeout, False) Then
                    Dim t As String = port.ReadExisting()
                    buffer &= t
                Else
                    If buffer.Length > 0 Then
                        Throw New ApplicationException("Response received is incomplete.")
                    Else
                        Throw New ApplicationException("No data received from phone.")
                    End If
                End If
            Loop While (Not buffer.EndsWith("\r\nOK\r\n") And Not buffer.EndsWith("\r\n> ") And Not buffer.EndsWith("\r\nERROR\r\n"))
        Catch ex As Exception
            Throw ex
        End Try

        Return buffer
    End Function

    Public Function ExecCommand(port As SerialPort, command As String, responseTimeout As Integer, errorMessage As String)
        Try
            port.DiscardOutBuffer()
            port.DiscardInBuffer()
            receiveNow.Reset()
            port.Write(command + "\r")

            Dim input As String = ReadResponse(port, responseTimeout)
            If ((input.Length = 0) Or Not (input.EndsWith("\r\nOK\r\n") Or input.EndsWith("\r\n> "))) Then
                Throw New ApplicationException("No success message was received.")
            End If
            Return input
        Catch ex As Exception
            Throw New ApplicationException(errorMessage, ex)
        End Try
    End Function

    Public Function SendMessage(port As SerialPort, PhoneNo As String, Message As String)
        Dim isSend As Boolean = False
        Try
            Dim recievedData As String = ExecCommand(port, "AT", 300, "No phone connected")
            recievedData = ExecCommand(port, "AT+CMGF=1", 300, "Failed to set message format.")
            Dim command As String = "AT+CMGS=\"" + PhoneNo + " \ ""
            recievedData = ExecCommand(port, command, 300, "Failed to accept phoneNo")
            command = Message & Char.ConvertFromUtf32(26) & "\r"
            recievedData = ExecCommand(port, command, 20000, "Failed to send message")
            If recievedData.EndsWith("\r\nOK\r\n") Then
                isSend = True
            ElseIf recievedData.Contains("ERROR") Then
                isSend = False
            End If
            General.LogActivity("SendSMS_", String.Format("{0}  {1}  {2}", PhoneNo, Message, isSend))
            Return isSend

        Catch ex As Exception
            General.LogActivity("SendSMS_", String.Format("{0}  {1}  {2}  {3}", PhoneNo, Message, isSend, ex.Message))
            Throw ex
        End Try
    End Function

    Public Sub ClosePort(ByRef port As SerialPort)
        Try
            port.Close()
            RemoveHandler port.DataReceived, AddressOf port_DataReceived
            port = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
