﻿Imports System.IO

Public Class General

    Public Shared Sub ErrorMessage(ByVal message As String)
        MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Sub

    Public Shared Function GetSetting(ByVal name As String)
        Dim database As DatabaseEntities = New DatabaseEntities()
        Dim setting = (From s In database.setting
                               Where s.nama.Equals(name)
                               Select s).FirstOrDefault()

        If setting.value_type = 0 Then
            Return setting.int
        ElseIf setting.value_type = 1 Then
            Return setting.decimal
        ElseIf setting.value_type = 2 Then
            Return setting.varchar
        ElseIf setting.value_type = 3 Then
            Return setting.text
        Else
            Return Nothing
        End If
    End Function

    Public Shared Sub LogActivity(sPathName As String, message As String)
        Dim sw As StreamWriter = Nothing
        Try
            If (Not System.IO.Directory.Exists("log\\")) Then
                System.IO.Directory.CreateDirectory("log\\")
            End If

            Dim sLogFormat As String = DateTime.Now.ToShortDateString().ToString() & " " & DateTime.Now.ToLongTimeString().ToString() & " ==> "
            Dim sYear As String = DateTime.Now.Year.ToString()
            Dim sMonth As String = DateTime.Now.Month.ToString()
            Dim sDay As String = DateTime.Now.Day.ToString()

            Dim sErrorTime As String = sDay & "-" & sMonth & "-" & sYear
            sw = New StreamWriter("log\\" & sPathName & sErrorTime & ".txt", True)
            sw.WriteLine(sLogFormat + message)
            sw.Flush()
        Catch ex As Exception
        Finally
            If Not IsNothing(sw) Then
                sw.Dispose()
                sw.Close()
            End If

        End Try
    End Sub

    Public Shared Sub ErrorLog(Message As String)
        Dim sw As StreamWriter = Nothing
        Try
            If (Not System.IO.Directory.Exists("log\\")) Then
                System.IO.Directory.CreateDirectory("log\\")
            End If

            Dim sLogFormat As String = DateTime.Now.ToShortDateString().ToString() & " " & DateTime.Now.ToLongTimeString().ToString() & " ==> "
            Dim sPathName As String = "log\\SMSapplicationErrorLog_"

            Dim sYear As String = DateTime.Now.Year.ToString()
            Dim sMonth As String = DateTime.Now.Month.ToString()
            Dim sDay As String = DateTime.Now.Day.ToString()

            Dim sErrorTime As String = sDay & "-" & sMonth & "-" & sYear

            sw = New StreamWriter(sPathName & sErrorTime & ".txt", True)

            sw.WriteLine(sLogFormat & Message)
            sw.Flush()
        Catch ex As Exception
            If Not IsNothing(sw) Then
                sw.Dispose()
                sw.Close()
            End If
        End Try
    End Sub
End Class
