﻿Imports System.IO.Ports
Imports System.Text.RegularExpressions
Imports System.Threading

Public Class SMSBackgroundProcess
    Public port As SerialPort
    Public smsLib As ATLib.SMSLib
    Public SIM_STORAGE As Boolean

    Dim database As DatabaseEntities

    Private Const CMD_DENDA As Integer = 1
    Private Const CMD_BUKU As Integer = 2

    Public Sub New()
        smsLib = New ATLib.SMSLib()
        SIM_STORAGE = True
        database = New DatabaseEntities()
    End Sub

    Private Function ParseMessages(input As String)
        Dim messages As List(Of ATLib.ShortMessage) = New List(Of ATLib.ShortMessage)()
        Dim r As Regex = New Regex("\+CMGL: (\d+),""(.+)"",""(.+)"",(.*),""(.+)""\r\n(.+)\r\n")
        Dim m As Match = r.Match(input)
        While (m.Success)
            Dim msg As ATLib.ShortMessage = New ATLib.ShortMessage()
            msg.Index = Integer.Parse(m.Groups(1).Value)
            msg.Status = m.Groups(2).Value
            msg.Sender = m.Groups(3).Value
            msg.Alphabet = m.Groups(4).Value
            msg.Sent = m.Groups(5).Value
            msg.Message = m.Groups(6).Value
            messages.Add(msg)

            m = m.NextMatch()
        End While
        Return messages
    End Function

    Public Sub ClosePort()
        smsLib.ClosePort(Me.port)
    End Sub

    Public Sub SendMultiPageSMS(sender As String, message As String)
        While message.Length > 160
            Dim part As String = message.Substring(0, 160)
            message = message.Substring(160)
            smsLib.SendMessage(Me.port, sender, part)
        End While
        smsLib.SendMessage(Me.port, sender, message)
    End Sub

    Public Sub ConfirmationBackgroundProcess()
        Dim bgProcess = New Thread(New ThreadStart(AddressOf ConfirmationProcess))
        bgProcess.IsBackground = True
        bgProcess.Start()
    End Sub

    Private Sub ConfirmationProcess()
        While True
            Try
                If Not IsNothing(Me.port) Then
                    'mengirim sms konfirmasi telah melakukan peminjaman buku
                    Dim listPeminjaman As List(Of peminjaman) = (From p In database.peminjaman
                                                                 Where p.konfirmasi = False
                                                                 Select p).ToList()
                    For Each item In listPeminjaman

                        General.LogActivity("ConfirmationProcess_",
                                String.Format("Receiver : {0}, NIM : {1}, No : {2}, Code Pemesanan : {3}",
                                              item.anggota.nama, item.anggota.nim, item.no_hp, item.code))
                        Dim tanggalTenggang As DateTime = DateTime.Parse(item.tanggal.ToString).AddDays(General.GetSetting("lama_peminjaman"))
                        Dim message As String = String.Format("Terima kasih telah melakukan peminjaman. Kode peminjaman Anda {0}. Tenggang waktu peminjaman {1}",
                                                              item.code, tanggalTenggang.ToShortDateString())
                        If smsLib.SendMessage(Me.port, item.no_hp, message) Then
                            item.konfirmasi = True
                            database.SaveChanges()
                            General.LogActivity("ConfirmationProcess_", "Success")
                        Else
                            General.LogActivity("ConfirmationProcess_", "Failed")
                        End If

                    Next

                    'mengirimkan sms peringatan tenggang waktu
                    listPeminjaman = (From p In database.peminjaman
                                      Where p.konfirmasi = True
                                      Select p).ToList()

                    For Each item In listPeminjaman
                        Dim currentDate As DateTime = DateTime.Now
                        Dim settingTime As Integer = General.GetSetting("lama_peminjaman")
                        Dim pDate As DateTime = DateTime.Parse(item.tanggal.ToString())
                        Dim range As Integer = (currentDate - pDate).TotalDays

                        'pengecekan waktu tenggang -1
                        If (range - settingTime) = 1 Then
                            General.LogActivity("RemainderProcess_",
                                String.Format("Receiver : {0}, NIM : {1}, No : {2}, Code Pemesanan : {3}",
                                              item.anggota.nama, item.anggota.nim, item.no_hp, item.code))

                            Dim message As String = String.Format("Masa peminjaman buku Anda dengan kode {0}," &
                                                                  "judul {1}, akan berakhir pada tanggal {2}. " &
                                                                  "Silahkan lakukan pengembalian buku.",
                                                                  item.code, item.buku.judul_buku, pDate.ToShortDateString())

                            If smsLib.SendMessage(Me.port, item.no_hp, message) Then
                                item.denda = True
                                database.SaveChanges()
                                General.LogActivity("ConfirmationProcess_", "Success")
                            Else
                                General.LogActivity("ConfirmationProcess_", "Failed")
                            End If

                            'pengecekan waktu tenggang pada hari ini
                        ElseIf (range - settingTime) = 0 Then
                            General.LogActivity("RemainderProcess_",
                                String.Format("Receiver : {0}, NIM : {1}, No : {2}, Code Pemesanan : {3}",
                                              item.anggota.nama, item.anggota.nim, item.no_hp, item.code))

                            Dim message As String = String.Format("Masa peminjaman buku Anda dengan kode {0}," &
                                                                  "judul {1}, akan berakhir pada hari ini {2}. " &
                                                                  "Silahkan lakukan pengembalian buku.",
                                                                  item.code, item.buku.judul_buku, pDate.ToShortDateString())

                            If smsLib.SendMessage(Me.port, item.no_hp, message) Then
                                item.denda = True
                                database.SaveChanges()
                                General.LogActivity("ConfirmationProcess_", "Success")
                            Else
                                General.LogActivity("ConfirmationProcess_", "Failed")
                            End If

                            'pengecekan waktu tenggang telah habis dan dikenakan denda
                        ElseIf (range - settingTime) > 0 Then
                            Dim settingDenda As Decimal = General.GetSetting("denda_per_hari")
                            Dim denda As Integer = (range - settingTime) * settingDenda

                            General.LogActivity("RemainderProcess_",
                                String.Format("Receiver : {0}, NIM : {1}, No : {2}, Code Pemesanan : {3}",
                                              item.anggota.nama, item.anggota.nim, item.no_hp, item.code))

                            Dim message As String = String.Format("Masa peminjaman buku Anda dengan kode {0}," &
                                                                  "judul {1}, telah berakhit pada tanggal {2}, dengan denda {3}" &
                                                                  "Silahkan lakukan pengembalian buku.",
                                                                  item.code, item.buku.judul_buku, pDate.ToShortDateString(), denda)

                            If smsLib.SendMessage(Me.port, item.no_hp, message) Then
                                item.denda = True
                                database.SaveChanges()
                                General.LogActivity("ConfirmationProcess_", "Success")
                            Else
                                General.LogActivity("ConfirmationProcess_", "Failed")
                            End If

                        End If
                    Next
                End If

            Catch ex As Exception
                General.ErrorLog(ex.Message)
            End Try
            Thread.Sleep(2000)
        End While
    End Sub
End Class
