﻿Public Class FrmSearchBook
    Dim database As DatabaseEntities
    Public book As buku

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
        book = New buku()
        LoadData()
    End Sub

    Private Sub LoadData()
        Dim search As String = txtCari.Text
        Dim data
        If search = "" Then
            data = (From b In database.buku
                   Select id_buku = b.id_buku,
                   judul_buku = b.judul_buku,
                   pengarang = b.pengarang,
                   penerbit = b.penerbit,
                   unit = b.unit,
                   status = If(b.status = True, "tersedia", "tidak tersedia")).ToList()
        Else
            data = (From b In database.buku
                    Where b.judul_buku.ToLower().Contains(search.ToLower()) Or b.pengarang.ToLower().Contains(search.ToLower())
                   Select id_buku = b.id_buku,
                   judul_buku = b.judul_buku,
                   pengarang = b.pengarang,
                   penerbit = b.penerbit,
                   unit = b.unit,
                   status = If(b.status = True, "tersedia", "tidak tersedia")).ToList()
        End If
        dataGrid.DataSource = data
    End Sub

    Private Sub BtnCari_Click(sender As System.Object, e As System.EventArgs) Handles BtnCari.Click
        LoadData()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub dataGrid_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dataGrid.CellDoubleClick
        Dim id_buku As Integer = dataGrid.Rows(e.RowIndex).Cells(0).Value

        book = (From b In database.buku
                Where b.id_buku = id_buku
                Select b).First()

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub
End Class