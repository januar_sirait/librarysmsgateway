﻿Public Class FormUtama
    Dim ucHome As UcHome
    Dim ucConnection As UcConnection
    Dim ucBook As UcBook
    Dim ucAnggota As UcAnggota
    Dim ucPeminjaman As UcPeminjaman
    Dim ucPengembalian As UcPengembalian

    Public smsBackgroundProcess As SMSBackgroundProcess

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        smsBackgroundProcess = New SMSBackgroundProcess()
        'smsBackgroundProcess.StartReadSMS()
        smsBackgroundProcess.ConfirmationBackgroundProcess()
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ucHome = New UcHome()
        ucHome.Dock = DockStyle.Fill
        ucHome.Location = New Point(0, 0)
        ucHome.Visible = True
        panelContent.Controls.Add(ucHome)

        ucConnection = New UcConnection()
        ucConnection.Dock = DockStyle.Fill
        ucConnection.Location = New Point(0, 0)
        ucConnection.Visible = True
        panelContent.Controls.Add(ucConnection)

        ucBook = New UcBook()
        ucBook.Dock = DockStyle.Fill
        ucBook.Location = New Point(0, 0)
        ucBook.Visible = True
        panelContent.Controls.Add(ucBook)

        ucAnggota = New UcAnggota()
        ucAnggota.Dock = DockStyle.Fill
        ucAnggota.Location = New Point(0, 0)
        ucAnggota.Visible = True
        panelContent.Controls.Add(ucAnggota)

        ucPeminjaman = New UcPeminjaman()
        ucPeminjaman.Dock = DockStyle.Fill
        ucPeminjaman.Location = New Point(0, 0)
        ucPeminjaman.Visible = True
        panelContent.Controls.Add(ucPeminjaman)

        ucPengembalian = New UcPengembalian()
        ucPengembalian.Dock = DockStyle.Fill
        ucPengembalian.Location = New Point(0, 0)
        ucPengembalian.Visible = True
        panelContent.Controls.Add(ucPengembalian)

        RemoveUC()
        ucConnection.Visible = True

    End Sub

    Private Sub RemoveUC()
        For Each item As Control In panelContent.Controls
            If item.GetType().BaseType.FullName.Equals("System.Windows.Forms.UserControl") Then
                Dim uc As UserControl
                uc = CType(item, UserControl)
                uc.Visible = False
            End If
        Next
    End Sub

    Private Sub tsbConnection_Click(sender As System.Object, e As System.EventArgs) Handles tsbConnection.Click
        If Not ucConnection.Visible Then
            RemoveUC()
            ucConnection.Visible = True
        End If
    End Sub

    Private Sub tsbHome_Click(sender As System.Object, e As System.EventArgs) Handles tsbHome.Click
        If Not ucHome.Visible Then
            RemoveUC()
            ucHome.Visible = True
        End If
    End Sub

    Private Sub tsbBuku_Click(sender As System.Object, e As System.EventArgs) Handles tsbBuku.Click
        If Not ucBook.Visible Then
            RemoveUC()
            ucBook.Visible = True
            ucBook.LoadData()
        End If
    End Sub

    Private Sub tsbAnggota_Click(sender As System.Object, e As System.EventArgs) Handles tsbAnggota.Click
        If Not ucAnggota.Visible Then
            RemoveUC()
            ucAnggota.Visible = True
            ucAnggota.LoadData()
        End If
    End Sub

    Private Sub tsbPeminjaman_Click(sender As System.Object, e As System.EventArgs) Handles tsbPeminjaman.Click
        If Not ucPeminjaman.Visible Then
            RemoveUC()
            ucPeminjaman.Visible = True
            ucPeminjaman.LoadData()
        End If
    End Sub

    Private Sub tsbPengembalian_Click(sender As System.Object, e As System.EventArgs) Handles tsbPengembalian.Click
        If Not ucPengembalian.Visible Then
            RemoveUC()
            ucPengembalian.Visible = True
            ucPengembalian.LoadData()
        End If
    End Sub

    Private Sub FormUtama_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        If Not IsNothing(smsBackgroundProcess.port) Then
            smsBackgroundProcess.ClosePort()
        End If
    End Sub
End Class
