﻿Public Class FrmAddPeminjaman
    Dim database As DatabaseEntities
    Dim book As buku
    Dim anggota As anggota

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
    End Sub

    Private Sub btnBatal_Click(sender As System.Object, e As System.EventArgs) Handles btnBatal.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If IsNothing(book) Then
            General.ErrorMessage("Silihkan pilih buku yang akan dipinjam!")
            Return
        End If

        If IsNothing(anggota) Then
            General.ErrorMessage("Silahkan pilih anggota yang meminjam buku!")
            Return
        End If

        If book.status = False Then
            General.ErrorMessage("Buku tidak tersedia")
            Return
        End If

        Dim peminjaman As peminjaman = New peminjaman()
        peminjaman.code = txtCode.Text
        peminjaman.tanggal = dtpTanggal.Value
        peminjaman.id_anggota = anggota.id_anggota
        peminjaman.id_buku = book.id_buku
        peminjaman.no_hp = txtAnggotaHp.Text
        peminjaman.konfirmasi = False
        database.peminjaman.AddObject(peminjaman)

        book.status = False
        database.ObjectStateManager.ChangeObjectState(book, EntityState.Modified)
        database.SaveChanges()

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub FrmAddPeminjaman_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim code As String = "P" & DateTime.Now.ToString("MM")

        Dim data = (From p In database.peminjaman
                    Where p.code.ToLower.StartsWith(code.ToLower)
                    Order By p.code Descending
                    Select p).FirstOrDefault()
        If IsNothing(data) Then
            code = code & "0001"
        Else
            Dim temp As Integer = Integer.Parse(data.code.Replace(code, ""))
            code = code & (temp + 1).ToString().PadLeft(4, "0")
        End If

        txtCode.Text = code
    End Sub

    Private Sub pctSearchBuku_Click(sender As System.Object, e As System.EventArgs) Handles pctSearchBuku.Click
        Dim frmSearchBook As FrmSearchBook = New FrmSearchBook()
        Dim result As DialogResult = frmSearchBook.ShowDialog()

        If result = Windows.Forms.DialogResult.OK Then
            book = (From b In database.buku
                    Where b.id_buku = frmSearchBook.book.id_buku
                    Select b).FirstOrDefault()
            txtBukuJudul.Text = book.judul_buku
            txtBukuPengarang.Text = book.pengarang()
            txtBukuPenerbit.Text = book.penerbit
            txtBukuUnit.Text = book.unit
        End If
    End Sub

    Private Sub pctSearchAnggota_Click(sender As System.Object, e As System.EventArgs) Handles pctSearchAnggota.Click
        Dim frmSearchAnggota As FrmSearchAnggota = New FrmSearchAnggota()
        Dim result As DialogResult = frmSearchAnggota.ShowDialog()

        If result = Windows.Forms.DialogResult.OK Then
            anggota = frmSearchAnggota.anggota
            txtAnggotaNim.Text = anggota.nim
            txtAnggotaNama.Text = anggota.nama
            txtAnggotaFakultas.Text = anggota.prodi.fakultas.nama
            txtAnggotaProdi.Text = anggota.prodi.nama
            txtAnggotaHp.Text = anggota.no_hp
        End If
    End Sub
End Class