﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UcConnection
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gboPortSetting = New System.Windows.Forms.GroupBox()
        Me.cmbWriteTimeout = New System.Windows.Forms.TextBox()
        Me.cmbReadTimeout = New System.Windows.Forms.TextBox()
        Me.cmbParityBits = New System.Windows.Forms.ComboBox()
        Me.cmbStopBits = New System.Windows.Forms.ComboBox()
        Me.cmbDataBits = New System.Windows.Forms.ComboBox()
        Me.cmbBaudRate = New System.Windows.Forms.ComboBox()
        Me.label7 = New System.Windows.Forms.Label()
        Me.label6 = New System.Windows.Forms.Label()
        Me.label5 = New System.Windows.Forms.Label()
        Me.label4 = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.cmbPort = New System.Windows.Forms.ComboBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.rdbDevice = New System.Windows.Forms.RadioButton()
        Me.rdbSIM = New System.Windows.Forms.RadioButton()
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.gboPortSetting.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gboPortSetting
        '
        Me.gboPortSetting.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.gboPortSetting.Controls.Add(Me.cmbWriteTimeout)
        Me.gboPortSetting.Controls.Add(Me.cmbReadTimeout)
        Me.gboPortSetting.Controls.Add(Me.cmbParityBits)
        Me.gboPortSetting.Controls.Add(Me.cmbStopBits)
        Me.gboPortSetting.Controls.Add(Me.cmbDataBits)
        Me.gboPortSetting.Controls.Add(Me.cmbBaudRate)
        Me.gboPortSetting.Controls.Add(Me.label7)
        Me.gboPortSetting.Controls.Add(Me.label6)
        Me.gboPortSetting.Controls.Add(Me.label5)
        Me.gboPortSetting.Controls.Add(Me.label4)
        Me.gboPortSetting.Controls.Add(Me.label3)
        Me.gboPortSetting.Controls.Add(Me.label2)
        Me.gboPortSetting.Controls.Add(Me.cmbPort)
        Me.gboPortSetting.Controls.Add(Me.label1)
        Me.gboPortSetting.Location = New System.Drawing.Point(408, 119)
        Me.gboPortSetting.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.gboPortSetting.Name = "gboPortSetting"
        Me.gboPortSetting.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.gboPortSetting.Size = New System.Drawing.Size(442, 338)
        Me.gboPortSetting.TabIndex = 34
        Me.gboPortSetting.TabStop = False
        Me.gboPortSetting.Text = "Port Setting"
        '
        'cmbWriteTimeout
        '
        Me.cmbWriteTimeout.Location = New System.Drawing.Point(204, 278)
        Me.cmbWriteTimeout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cmbWriteTimeout.Name = "cmbWriteTimeout"
        Me.cmbWriteTimeout.Size = New System.Drawing.Size(180, 26)
        Me.cmbWriteTimeout.TabIndex = 42
        Me.cmbWriteTimeout.Text = "300"
        '
        'cmbReadTimeout
        '
        Me.cmbReadTimeout.Location = New System.Drawing.Point(204, 238)
        Me.cmbReadTimeout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cmbReadTimeout.Name = "cmbReadTimeout"
        Me.cmbReadTimeout.Size = New System.Drawing.Size(180, 26)
        Me.cmbReadTimeout.TabIndex = 41
        Me.cmbReadTimeout.Text = "300"
        '
        'cmbParityBits
        '
        Me.cmbParityBits.FormattingEnabled = True
        Me.cmbParityBits.Items.AddRange(New Object() {"Even", "Odd", "None"})
        Me.cmbParityBits.Location = New System.Drawing.Point(204, 197)
        Me.cmbParityBits.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cmbParityBits.Name = "cmbParityBits"
        Me.cmbParityBits.Size = New System.Drawing.Size(180, 28)
        Me.cmbParityBits.TabIndex = 40
        Me.cmbParityBits.Text = "None"
        '
        'cmbStopBits
        '
        Me.cmbStopBits.FormattingEnabled = True
        Me.cmbStopBits.Items.AddRange(New Object() {"1", "1.5", "2"})
        Me.cmbStopBits.Location = New System.Drawing.Point(204, 155)
        Me.cmbStopBits.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cmbStopBits.Name = "cmbStopBits"
        Me.cmbStopBits.Size = New System.Drawing.Size(180, 28)
        Me.cmbStopBits.TabIndex = 39
        Me.cmbStopBits.Text = "1"
        '
        'cmbDataBits
        '
        Me.cmbDataBits.FormattingEnabled = True
        Me.cmbDataBits.Items.AddRange(New Object() {"5", "6", "7", "8"})
        Me.cmbDataBits.Location = New System.Drawing.Point(204, 112)
        Me.cmbDataBits.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cmbDataBits.Name = "cmbDataBits"
        Me.cmbDataBits.Size = New System.Drawing.Size(180, 28)
        Me.cmbDataBits.TabIndex = 38
        Me.cmbDataBits.Text = "8"
        '
        'cmbBaudRate
        '
        Me.cmbBaudRate.FormattingEnabled = True
        Me.cmbBaudRate.Items.AddRange(New Object() {"110", "300", "1200", "2400", "4800", "9600", "19200", "38400", "57600", "115200", "230400", "460800", "921600"})
        Me.cmbBaudRate.Location = New System.Drawing.Point(204, 71)
        Me.cmbBaudRate.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cmbBaudRate.Name = "cmbBaudRate"
        Me.cmbBaudRate.Size = New System.Drawing.Size(180, 28)
        Me.cmbBaudRate.TabIndex = 37
        Me.cmbBaudRate.Text = "9600"
        '
        'label7
        '
        Me.label7.AutoSize = True
        Me.label7.Location = New System.Drawing.Point(57, 283)
        Me.label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(107, 20)
        Me.label7.TabIndex = 36
        Me.label7.Text = "Write Timeout"
        '
        'label6
        '
        Me.label6.AutoSize = True
        Me.label6.Location = New System.Drawing.Point(57, 243)
        Me.label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(109, 20)
        Me.label6.TabIndex = 35
        Me.label6.Text = "Read Timeout"
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Location = New System.Drawing.Point(57, 202)
        Me.label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(79, 20)
        Me.label5.TabIndex = 34
        Me.label5.Text = "Parity Bits"
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Location = New System.Drawing.Point(57, 160)
        Me.label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(74, 20)
        Me.label4.TabIndex = 33
        Me.label4.Text = "Stop Bits"
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(57, 117)
        Me.label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(75, 20)
        Me.label3.TabIndex = 32
        Me.label3.Text = "Data Bits"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(57, 75)
        Me.label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(86, 20)
        Me.label2.TabIndex = 31
        Me.label2.Text = "Baud Rate"
        '
        'cmbPort
        '
        Me.cmbPort.FormattingEnabled = True
        Me.cmbPort.Location = New System.Drawing.Point(204, 31)
        Me.cmbPort.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cmbPort.Name = "cmbPort"
        Me.cmbPort.Size = New System.Drawing.Size(180, 28)
        Me.cmbPort.TabIndex = 30
        '
        'label1
        '
        Me.label1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(57, 35)
        Me.label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(84, 20)
        Me.label1.TabIndex = 29
        Me.label1.Text = "Port Name"
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel1.Controls.Add(Me.rdbDevice)
        Me.Panel1.Controls.Add(Me.rdbSIM)
        Me.Panel1.Controls.Add(Me.btnConnect)
        Me.Panel1.Location = New System.Drawing.Point(408, 465)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(442, 152)
        Me.Panel1.TabIndex = 35
        '
        'rdbDevice
        '
        Me.rdbDevice.AutoSize = True
        Me.rdbDevice.Location = New System.Drawing.Point(256, 33)
        Me.rdbDevice.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.rdbDevice.Name = "rdbDevice"
        Me.rdbDevice.Size = New System.Drawing.Size(143, 24)
        Me.rdbDevice.TabIndex = 39
        Me.rdbDevice.TabStop = True
        Me.rdbDevice.Text = "Device Storage"
        Me.rdbDevice.UseVisualStyleBackColor = True
        '
        'rdbSIM
        '
        Me.rdbSIM.AutoSize = True
        Me.rdbSIM.Location = New System.Drawing.Point(44, 33)
        Me.rdbSIM.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.rdbSIM.Name = "rdbSIM"
        Me.rdbSIM.Size = New System.Drawing.Size(124, 24)
        Me.rdbSIM.TabIndex = 38
        Me.rdbSIM.TabStop = True
        Me.rdbSIM.Text = "SIM Storage"
        Me.rdbSIM.UseVisualStyleBackColor = True
        '
        'btnConnect
        '
        Me.btnConnect.Location = New System.Drawing.Point(159, 82)
        Me.btnConnect.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(112, 35)
        Me.btnConnect.TabIndex = 37
        Me.btnConnect.Text = "Connect"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'UcConnection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.gboPortSetting)
        Me.Name = "UcConnection"
        Me.Size = New System.Drawing.Size(1258, 650)
        Me.gboPortSetting.ResumeLayout(False)
        Me.gboPortSetting.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents gboPortSetting As System.Windows.Forms.GroupBox
    Private WithEvents cmbWriteTimeout As System.Windows.Forms.TextBox
    Private WithEvents cmbReadTimeout As System.Windows.Forms.TextBox
    Private WithEvents cmbParityBits As System.Windows.Forms.ComboBox
    Private WithEvents cmbStopBits As System.Windows.Forms.ComboBox
    Private WithEvents cmbDataBits As System.Windows.Forms.ComboBox
    Private WithEvents cmbBaudRate As System.Windows.Forms.ComboBox
    Private WithEvents label7 As System.Windows.Forms.Label
    Private WithEvents label6 As System.Windows.Forms.Label
    Private WithEvents label5 As System.Windows.Forms.Label
    Private WithEvents label4 As System.Windows.Forms.Label
    Private WithEvents label3 As System.Windows.Forms.Label
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents cmbPort As System.Windows.Forms.ComboBox
    Private WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Private WithEvents rdbDevice As System.Windows.Forms.RadioButton
    Private WithEvents rdbSIM As System.Windows.Forms.RadioButton
    Private WithEvents btnConnect As System.Windows.Forms.Button

End Class
