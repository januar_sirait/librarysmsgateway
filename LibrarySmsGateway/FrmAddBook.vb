﻿Public Class FrmAddBook
    Dim database As DatabaseEntities

    Private Sub FrmAddBook_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        database = New DatabaseEntities()
    End Sub

    Private Sub btnBatal_Click(sender As System.Object, e As System.EventArgs) Handles btnBatal.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If txtJudul.Text = "" Then
            General.ErrorMessage("Judul buku tidak boleh kosong!")
            Return
        End If

        If txtPengarang.Text = "" Then
            General.ErrorMessage("Pengarang buku tidak boleh kosong!")
            Return
        End If

        If txtPenerbit.Text = "" Then
            General.ErrorMessage("Penerbit buku tidak boleh kosong!")
            Return
        End If

        Dim jumlah As Integer
        If txtJumlah.Text = "" Or Not Integer.TryParse(txtJumlah.Text, jumlah) Then
            General.ErrorMessage("Jumlah buku tidak valid!")
            Return
        End If

        For i As Integer = 1 To jumlah
            Dim book As buku = New buku()
            book.judul_buku = txtJudul.Text
            book.pengarang = txtPengarang.Text
            book.penerbit = txtPenerbit.Text
            book.unit = i
            book.status = True
            database.AddTobuku(book)
        Next
        database.SaveChanges()

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub
End Class