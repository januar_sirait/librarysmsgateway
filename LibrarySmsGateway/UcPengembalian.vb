﻿Public Class UcPengembalian
    Dim database As DatabaseEntities

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
    End Sub

    Public Sub LoadData()
        Dim search As String = txtCari.Text
        Dim data
        If search = "" Then
            data = (From p In database.pengembalian
                   Select id_pengembalian = p.id_pengembalian,
                   code = p.peminjaman.code,
                   tanggal_peminjaman = p.peminjaman.tanggal,
                   tanggal_pengembalian = p.tanggal,
                   judul_buku = p.peminjaman.buku.judul_buku,
                   pengarang = p.peminjaman.buku.pengarang,
                   penerbit = p.peminjaman.buku.penerbit,
                   nama = p.peminjaman.anggota.nama,
                   nim = p.peminjaman.anggota.nim,
                   denda = p.denda).ToList()
        Else
            data = (From p In database.pengembalian
                    Where p.peminjaman.code.ToLower.Contains(search.ToLower()) Or p.peminjaman.anggota.nama.ToLower.Contains(search.ToLower)
                   Select id_pengembalian = p.id_pengembalian,
                   code = p.peminjaman.code,
                   tanggal_peminjaman = p.peminjaman.tanggal,
                   tanggal_pengembalian = p.tanggal,
                   judul_buku = p.peminjaman.buku.judul_buku,
                   pengarang = p.peminjaman.buku.pengarang,
                   penerbit = p.peminjaman.buku.penerbit,
                   nama = p.peminjaman.anggota.nama,
                   nim = p.peminjaman.anggota.nim,
                   denda = p.denda).ToList()
        End If
        dataGrid.DataSource = data
    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click
        Dim frmPengembalian As FrmPengembalian = New FrmPengembalian()
        Dim result As DialogResult = frmPengembalian.ShowDialog()

        If result = DialogResult.OK Then
            LoadData()
        End If
    End Sub
End Class
