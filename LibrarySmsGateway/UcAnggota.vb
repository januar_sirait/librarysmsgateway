﻿Public Class UcAnggota
    Dim database As DatabaseEntities

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
    End Sub

    Public Sub LoadData()
        Dim search As String = txtCari.Text
        Dim data
        If search = "" Then
            data = (From a In database.anggota
                   Select id_anggota = a.id_anggota,
                   nim = a.nim,
                   nama = a.nama,
                   fakultas = a.prodi.fakultas.nama,
                   prodi = a.prodi.nama,
                   tempat_lahir = a.tempat_lahir,
                   tanggal_lahir = a.tanggal_lahir,
                   no_hp = a.no_hp).ToList()
        Else
            data = (From a In database.anggota
                    Where a.nama.ToLower().Contains(search.ToLower())
                   Select id_anggota = a.id_anggota,
                   nim = a.nim,
                   nama = a.nama,
                   fakultas = a.prodi.fakultas.nama,
                   prodi = a.prodi.nama,
                   tempat_lahir = a.tempat_lahir,
                   tanggal_lahir = a.tanggal_lahir,
                   no_hp = a.no_hp).ToList()
        End If
        dataGrid.DataSource = data
    End Sub

    Private Sub UcAnggota_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadData()
    End Sub

    Private Sub btnCari_Click(sender As System.Object, e As System.EventArgs) Handles btnCari.Click
        LoadData()
    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click
        Dim frmAddAnggota As FrmAddAnggota = New FrmAddAnggota()
        Dim result As DialogResult = frmAddAnggota.ShowDialog()

        If result = DialogResult.OK Then
            LoadData()
        End If
    End Sub

    Private Sub dataGrid_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dataGrid.CellContentDoubleClick
        Dim id_anggota As Integer = dataGrid.Rows(e.RowIndex).Cells(0).Value
        Dim anggota As anggota = (From a In database.anggota
                                 Where a.id_anggota = id_anggota
                                 Select a).FirstOrDefault()

        Dim frmEditAnggota As FrmEditAnggota = New FrmEditAnggota()
        Dim result As DialogResult
        frmEditAnggota.anggota = anggota
        result = frmEditAnggota.ShowDialog()

        If result = DialogResult.OK Then
            LoadData()
        End If
    End Sub
End Class
