﻿Public Class FrmPengembalian
    Dim database As DatabaseEntities
    Dim peminjaman As peminjaman

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
    End Sub

    Private Sub FrmPengembalian_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim listCode = (From p In database.peminjaman
                        Where p.pengembalian.Count = 0
                        Select p).ToList()
        cmbCode.DataSource = listCode
        cmbCode.ValueMember = "code"
        cmbCode.SelectedIndex = -1

        txtTanggalPeminjaman.Text = ""
        txtDenda.Text = ""

        txtBukuJudul.Text = ""
        txtBukuPengarang.Text = ""
        txtBukuPenerbit.Text = ""
        txtBukuUnit.Text = ""

        txtAnggotaNim.Text = ""
        txtAnggotaNama.Text = ""
        txtAnggotaFakultas.Text = ""
        txtAnggotaProdi.Text = ""
        txtAnggotaHp.Text = ""
    End Sub

    Private Sub cmbCode_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbCode.SelectedIndexChanged
        If cmbCode.SelectedIndex > -1 Then
            Dim selectedCode As peminjaman = cmbCode.SelectedItem
            peminjaman = (From p In database.peminjaman
                            Where p.code.Equals(selectedCode.code)
                            Select p).FirstOrDefault()
            If Not IsNothing(peminjaman) Then
                Dim settingTime As Integer = General.GetSetting("lama_peminjaman")
                Dim settingDenda As Decimal = General.GetSetting("denda_per_hari")

                Dim denda As Integer
                Dim dateNow As DateTime = DateTime.Now
                Dim pDate As DateTime = DateTime.Parse(peminjaman.tanggal.ToString())
                Dim range As Integer = (dateNow - pDate).TotalDays
                If (range > settingTime) Then
                    denda = (range - settingTime) * settingDenda
                Else
                    denda = 0
                End If

                txtTanggalPeminjaman.Text = peminjaman.tanggal.ToString()
                txtDenda.Text = denda

                txtBukuJudul.Text = peminjaman.buku.judul_buku
                txtBukuPengarang.Text = peminjaman.buku.pengarang
                txtBukuPenerbit.Text = peminjaman.buku.penerbit
                txtBukuUnit.Text = peminjaman.buku.unit

                txtAnggotaNim.Text = peminjaman.anggota.nim
                txtAnggotaNama.Text = peminjaman.anggota.nama
                txtAnggotaFakultas.Text = peminjaman.anggota.prodi.fakultas.nama
                txtAnggotaProdi.Text = peminjaman.anggota.prodi.nama
                txtAnggotaHp.Text = peminjaman.anggota.no_hp
            End If

        End If
    End Sub

    Private Sub btnBatal_Click(sender As System.Object, e As System.EventArgs) Handles btnBatal.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If IsNothing(peminjaman) Then
            General.ErrorMessage("Silahkan pilih code peminjaman!")
            Return
        End If

        Dim pengembalian As pengembalian = New pengembalian()
        pengembalian.id_peminjaman = peminjaman.id_peminjaman
        pengembalian.tanggal = DateTime.Now
        pengembalian.denda = Decimal.Parse(txtDenda.Text)

        database.pengembalian.AddObject(pengembalian)

        Dim book = (From b In database.buku
                    Where b.id_buku = peminjaman.id_buku
                    Select b).FirstOrDefault()
        book.status = True

        database.SaveChanges()

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub
End Class