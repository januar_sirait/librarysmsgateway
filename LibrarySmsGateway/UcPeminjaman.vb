﻿Public Class UcPeminjaman
    Dim database As DatabaseEntities

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
    End Sub


    Private Sub UcPeminjaman_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub LoadData()
        Dim search As String = txtCari.Text
        Dim data
        If search = "" Then
            data = (From p In database.peminjaman
                   Select id_peminjaman = p.id_peminjaman,
                   code = p.code,
                   tanggal = p.tanggal,
                   judul_buku = p.buku.judul_buku,
                   pengarang = p.buku.pengarang,
                   penerbit = p.buku.penerbit,
                   nama = p.anggota.nama,
                   nim = p.anggota.nim).ToList()
        Else
            data = (From p In database.peminjaman
                    Where p.code.ToLower.Contains(search.ToLower()) Or p.anggota.nama.ToLower.Contains(search.ToLower)
                   Select id_peminjaman = p.id_peminjaman,
                   code = p.code,
                   tanggal = p.tanggal,
                   judul_buku = p.buku.judul_buku,
                   pengarang = p.buku.pengarang,
                   penerbit = p.buku.penerbit,
                   nama = p.anggota.nama,
                   nim = p.anggota.nim).ToList()
        End If
        dataGrid.DataSource = data
    End Sub

    Private Sub btnCari_Click(sender As System.Object, e As System.EventArgs) Handles btnCari.Click
        LoadData()
    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click

        Dim frmAddPeminjaman As FrmAddPeminjaman = New FrmAddPeminjaman()
        Dim result As DialogResult = frmAddPeminjaman.ShowDialog()

        If result = DialogResult.OK Then
            LoadData()
        End If
    End Sub
End Class
