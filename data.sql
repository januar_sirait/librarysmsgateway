/*
SQLyog Enterprise v10.42 
MySQL - 5.5.5-10.1.10-MariaDB : Database - library_sms_gateway
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`library_sms_gateway` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `library_sms_gateway`;

/*Table structure for table `anggota` */

DROP TABLE IF EXISTS `anggota`;

CREATE TABLE `anggota` (
  `id_anggota` int(11) NOT NULL AUTO_INCREMENT,
  `id_prodi` int(11) DEFAULT NULL,
  `nim` varchar(15) DEFAULT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `tempat_lahir` varchar(15) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id_anggota`),
  KEY `id_prodi` (`id_prodi`),
  CONSTRAINT `anggota_ibfk_1` FOREIGN KEY (`id_prodi`) REFERENCES `prodi` (`id_prodi`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `anggota` */

insert  into `anggota`(`id_anggota`,`id_prodi`,`nim`,`nama`,`tempat_lahir`,`tanggal_lahir`,`no_hp`) values (1,54,'141421044','Januar Sirait','Medan','1991-01-21','085760534549'),(2,54,'141421037','Josua Pribadi Sianipar','Medan','1991-05-08','085760534549');

/*Table structure for table `buku` */

DROP TABLE IF EXISTS `buku`;

CREATE TABLE `buku` (
  `id_buku` int(11) NOT NULL AUTO_INCREMENT,
  `judul_buku` varchar(250) DEFAULT NULL,
  `pengarang` varchar(250) DEFAULT NULL,
  `penerbit` varchar(50) DEFAULT NULL,
  `unit` int(2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_buku`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `buku` */

insert  into `buku`(`id_buku`,`judul_buku`,`pengarang`,`penerbit`,`unit`,`status`) values (1,'Head First Java','Bert Bates, Kathy Sierra','O\'Reilly',1,0),(2,'Head First Java','Bert Bates, Kathy Sierra','O\'Reilly',2,1),(3,'Head First Java','Bert Bates, Kathy Sierra','O\'Reilly',3,1),(4,'Head First C#','Andrew Stellman','O\'Reilly',1,1),(5,'Head First C#','Andrew Stellman','O\'Reilly',2,1),(6,'Head First JavaScript','Michael Morrison','O\'Reilly',1,1),(7,'Head First JavaScript','Michael Morrison','O\'Reilly',2,1),(8,'Head First JavaScript','Michael Morrison','O\'Reilly',3,1);

/*Table structure for table `fakultas` */

DROP TABLE IF EXISTS `fakultas`;

CREATE TABLE `fakultas` (
  `id_fakultas` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_fakultas`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `fakultas` */

insert  into `fakultas`(`id_fakultas`,`nama`) values (1,'FAKULTAS KEDOKTERAN'),(2,'FAKULTAS HUKUM'),(3,'FAKULTAS PERTANIAN'),(4,'FAKULTAS TEKNIK'),(5,'FAKULTAS EKONOMI'),(6,'FAKULTAS KEDOKTERAN GIGI'),(7,'FAKULTAS SASTRA'),(8,'FAKULTAS MATEMATIKA DAN ILMU PENGETAHUAN ALAM'),(9,'ILMU-ILMU SOSIAL DAN ILMU POLITIK'),(10,'FAKULTAS KESEHATAN MASYARAKAT'),(11,'FAKULTAS PSIKOLOGI'),(12,'FAKULTAS FARMASI'),(13,'FAKULTAS KEPERAWATAN'),(14,'SEKOLAH PASCASARJANA'),(15,'FAKULTAS ILMU KOMPUTER DAN TEKNOLOGI INFORMASI');

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `logs` */

/*Table structure for table `peminjaman` */

DROP TABLE IF EXISTS `peminjaman`;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(7) DEFAULT NULL,
  `id_anggota` int(11) DEFAULT NULL,
  `id_buku` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `konfirmasi` tinyint(1) DEFAULT '0',
  `denda` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_anggota` (`id_anggota`),
  KEY `id_buku` (`id_buku`),
  CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `peminjaman_ibfk_2` FOREIGN KEY (`id_buku`) REFERENCES `buku` (`id_buku`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `peminjaman` */

insert  into `peminjaman`(`id_peminjaman`,`code`,`id_anggota`,`id_buku`,`tanggal`,`no_hp`,`konfirmasi`,`denda`) values (2,'P050001',1,1,'2016-05-15','085760534549',0,NULL),(3,'P050002',1,4,'2016-05-16','085760534549',0,NULL);

/*Table structure for table `pengembalian` */

DROP TABLE IF EXISTS `pengembalian`;

CREATE TABLE `pengembalian` (
  `id_pengembalian` int(11) NOT NULL AUTO_INCREMENT,
  `id_peminjaman` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `denda` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_pengembalian`),
  KEY `id_peminjaman` (`id_peminjaman`),
  CONSTRAINT `pengembalian_ibfk_1` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `pengembalian` */

insert  into `pengembalian`(`id_pengembalian`,`id_peminjaman`,`tanggal`,`denda`) values (2,3,'2016-05-16',5000.00);

/*Table structure for table `prodi` */

DROP TABLE IF EXISTS `prodi`;

CREATE TABLE `prodi` (
  `id_prodi` int(11) NOT NULL AUTO_INCREMENT,
  `id_fakultas` int(11) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_prodi`),
  KEY `id_fakultas` (`id_fakultas`),
  CONSTRAINT `prodi_ibfk_1` FOREIGN KEY (`id_fakultas`) REFERENCES `fakultas` (`id_fakultas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

/*Data for the table `prodi` */

insert  into `prodi`(`id_prodi`,`id_fakultas`,`nama`) values (1,1,'Pendidikan Dokter'),(2,2,'Ilmu Hukum'),(3,3,'Agribisnis'),(4,3,'Agroekoteknologi'),(5,3,'Ilmu Peternakan'),(6,3,'Ilmu dan Teknologi Pangan'),(7,3,'Kehutanan'),(8,3,'Manajemen Sumberdaya Perairan'),(9,3,'Ilmu Pertanian'),(10,4,'Arsitektur'),(11,4,'Teknik Elektro'),(12,4,'Teknik Industri'),(13,4,'Teknik Kimia'),(14,4,'Teknik Mesin'),(15,4,'Teknik Sipil'),(16,5,'Akuntansi'),(17,5,'Ekonomi Pembangunan'),(18,5,'Manajemen'),(19,5,'Keuangan'),(20,5,'Kesekretariatan'),(21,6,'Pendidikan Dokter Gigi'),(22,7,'Sastra Arab'),(23,7,'Sastra Batak'),(24,7,'Sastra Indonesia'),(25,7,'Sastra Inggris'),(26,7,'Sastra Jepang'),(27,7,'Sastra Melayu'),(28,7,'Etnomusikologi'),(29,7,'Ilmu Perpustakaan dan Informasi'),(30,7,'Ilmu Sejarah'),(31,7,'Pariwisata'),(32,7,'Sastra Cina'),(33,7,'Penciptaan dan Pengkajian Seni'),(34,7,'Linguistik'),(35,8,'Matematika'),(36,8,'Kimia'),(37,8,'Fisika'),(38,8,'Biologi'),(39,9,'Ilmu Administrasi Negara '),(40,9,'Ilmu Politik '),(41,9,'Antropologi Sosial '),(42,9,'Ilmu Kesejahteraan Sosial '),(43,9,'Sosiologi'),(44,9,'Ilmu Komunikasi \r\nIlmu Administrasi Nia'),(45,9,'Ilmu Administrasi Niaga/Bisnis'),(46,10,'Ilmu Kesehatan Masyarakat'),(47,11,'Psikologi'),(48,12,'Farmasi - Profesi Apoteker'),(49,13,'Ilmu Keperawatan'),(50,14,'Perencanaan Wilayah dan Pedesaan '),(51,14,'Pengelolaan Sumberdaya Alam dan Lingkungan (PSL) '),(52,14,'Manajemen'),(53,14,'Manajemen Properti dan Penilaian'),(54,15,'Ilmu Komputer'),(55,15,'Teknologi Informasi');

/*Table structure for table `setting` */

DROP TABLE IF EXISTS `setting`;

CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `value_type` int(1) DEFAULT NULL,
  `int` int(11) DEFAULT NULL,
  `decimal` decimal(10,0) DEFAULT NULL,
  `varchar` varchar(250) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `setting` */

insert  into `setting`(`id_setting`,`nama`,`value_type`,`int`,`decimal`,`varchar`,`text`) values (1,'lama_peminjaman',0,10,NULL,NULL,NULL),(2,'denda_per_hari',1,NULL,1000,NULL,NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
